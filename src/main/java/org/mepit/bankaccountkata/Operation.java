package org.mepit.bankaccountkata;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

import static java.util.Objects.requireNonNull;

public class Operation {

    private final BigDecimal amount;
    private final ZonedDateTime dateTime;

    public Operation(BigDecimal amount, ZonedDateTime date) {
        this.amount = requireNonNull(amount);
        this.dateTime = requireNonNull(date);
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public ZonedDateTime getDate() {
        return dateTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Operation operation = (Operation) o;

        if (!amount.equals(operation.amount)) return false;
        if (!dateTime.equals(operation.dateTime)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = amount.hashCode();
        result = 31 * result + dateTime.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Operation{" +
                "amount=" + amount +
                ", dateTime=" + dateTime +
                '}';
    }
}
