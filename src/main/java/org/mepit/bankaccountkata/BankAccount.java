package org.mepit.bankaccountkata;


import java.math.BigDecimal;
import java.time.Clock;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import static java.math.BigDecimal.ZERO;
import static java.util.Collections.unmodifiableList;
import static java.util.Objects.requireNonNull;

public class BankAccount {

    private final Clock clock;
    private BigDecimal balance = ZERO;
    private final List<Operation> operations = new ArrayList<>();

    public BankAccount() {
        clock = Clock.systemUTC();
    }

    public BankAccount(Clock clock) {
        this.clock = clock;
    }

    public void makeADeposit(BigDecimal amount) {
        requireNonNull(amount);
        if (amount.compareTo(ZERO) < 0) {
            throw new IllegalArgumentException("Deposit amount must not be negative");
        }

        operations.add(createOperation(amount));
        balance = balance.add(amount);
    }

    public void makeAWithdrawal(BigDecimal amount) {
        requireNonNull(amount);
        if (amount.compareTo(ZERO) < 0) {
            throw new IllegalArgumentException("Withdrawal amount must not be negative");
        }

        if (balance.compareTo(amount) < 0) {
            throw new IllegalStateException("Not enough money");
        }

        BigDecimal negativeAmount = amount.negate();
        operations.add(createOperation(negativeAmount));
        balance = balance.add(negativeAmount);
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public List<Operation> getOperations() {
        return unmodifiableList(operations);
    }

    private Operation createOperation(BigDecimal negate) {
        return new Operation(negate, ZonedDateTime.now(clock));
    }
}

