package org.mepit.bankaccountkata;

import org.junit.Test;

import java.math.BigDecimal;
import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;

import static java.math.BigDecimal.TEN;
import static java.math.BigDecimal.ZERO;
import static org.assertj.core.api.Assertions.*;

public class BankAccountTest {

    @Test
    public void should_have_an_initial_balance_of_0() {
        // When
        BankAccount bankAccount = new BankAccount();

        // Then
        assertThat(bankAccount.getBalance()).isEqualTo(ZERO);
    }

    @Test
    public void should_make_a_deposit() {
        // Given
        BankAccount bankAccount = new BankAccount();

        // When
        bankAccount.makeADeposit(TEN);

        // Then
        assertThat(bankAccount.getBalance()).isEqualTo(TEN);
    }

    @Test
    public void should_not_make_a_deposit_having_a_null_amount() {
        // Given
        BankAccount bankAccount = new BankAccount();

        // Then
        assertThatNullPointerException().isThrownBy(() -> bankAccount.makeADeposit(null));
    }

    @Test
    public void should_not_make_a_deposit_having_a_negative_amount() {
        // Given
        BankAccount bankAccount = new BankAccount();

        // Then
        assertThatIllegalArgumentException()
                .isThrownBy(() -> bankAccount.makeADeposit(BigDecimal.valueOf(-1)))
                .withMessage("Deposit amount must not be negative");
    }

    @Test
    public void should_make_a_withdrawal() {
        // Given
        BankAccount bankAccount = new BankAccount();
        bankAccount.makeADeposit(new BigDecimal("110.50"));

        // When
        bankAccount.makeAWithdrawal(new BigDecimal("10.30"));

        // Then
        assertThat(bankAccount.getBalance()).isEqualTo(new BigDecimal("100.20"));
    }

    @Test
    public void should_not_make_a_withdrawal_having_a_null_amount() {
        // Given
        BankAccount bankAccount = new BankAccount();

        // Then
        assertThatNullPointerException().isThrownBy(() -> bankAccount.makeAWithdrawal(null));
    }

    @Test
    public void should_not_make_a_withdrawal_having_a_negative_amount() {
        // Given
        BankAccount bankAccount = new BankAccount();

        // Then
        assertThatIllegalArgumentException()
                .isThrownBy(() -> bankAccount.makeAWithdrawal(BigDecimal.valueOf(-1)))
                .withMessage("Withdrawal amount must not be negative");
    }

    @Test
    public void should_not_make_a_withdrawal_when_not_having_enough_money() {
        // Given
        BankAccount bankAccount = new BankAccount();

        // Then
        assertThatIllegalStateException()
                .isThrownBy(() -> bankAccount.makeAWithdrawal(TEN))
                .withMessage("Not enough money");
    }

    @Test
    public void should_not_have_any_operation_at_creation() {
        // Given
        BankAccount bankAccount = new BankAccount();

        // Then
        assertThat(bankAccount.getOperations()).isEmpty();
    }

    @Test
    public void should_not_have_mutable_operations() {
        // Given
        BankAccount bankAccount = new BankAccount();
        List<Operation> operations = bankAccount.getOperations();

        // Then
        assertThatExceptionOfType(UnsupportedOperationException.class)
                .isThrownBy(operations::clear);
    }

    @Test
    public void should_list_operations() {
        // Given
        BigDecimal depositAmount = new BigDecimal("2000.00");
        BigDecimal withdrawalAmount = new BigDecimal("99.99");

        Clock fixedClock = Clock.fixed(Instant.now(), ZoneId.systemDefault());
        ZonedDateTime expectedOperationDate = ZonedDateTime.ofInstant(fixedClock.instant(), fixedClock.getZone());

        BankAccount account = new BankAccount(fixedClock);
        account.makeADeposit(depositAmount);
        account.makeAWithdrawal(withdrawalAmount);

        // When
        List<Operation> operations = account.getOperations();

        // Then
        assertThat(operations).containsExactly(
                new Operation(depositAmount, expectedOperationDate),
                new Operation(withdrawalAmount.negate(), expectedOperationDate));
    }
}